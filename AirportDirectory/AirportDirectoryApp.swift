//
//  AirportDirectoryApp.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import SwiftUI

@main
struct AirportDirectoryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
