//
//  RunwayRow.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 10.01.2022.
//

import Foundation

struct RunwayRow: Decodable {
    
    var icao: String?
    var rw: String?
    var len: Int?
    var width: Int?
    var mc: String?
    var lat: String?
    var lon: String?
    var elev: Int?
    var dsplcd_thr: Int?
    var stopway: Int?
    var slope: String?
    
}
