//
//  SettingsSet.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 28.01.2022.
//

import Foundation

struct SettingsSet {
    var unit: DistanceUnits = DistanceUnits.meter
    var coord: CoordinateUnits = CoordinateUnits.ddmmss
}
