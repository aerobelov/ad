//
//  Units.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 12.01.2022.
//

import Foundation

enum DistanceUnits: Double, CaseIterable, Identifiable {
    case meter = 0.305
    case feet = 1
    
    var id: String {
        switch self{
            case .meter: return "m"
            case .feet: return "f"
        }
    }
}

enum CoordinateUnits:  CaseIterable, Identifiable {
    case ddmm
    case ddmmss
    var id: String {
        switch self{
            case .ddmm: return "DD.MM"
            case .ddmmss: return "DD:MM:SS"
        }
    }
}
