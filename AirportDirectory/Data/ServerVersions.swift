//
//  ServerVersions.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 29.01.2022.
//

import Foundation

class ServerVersion: Decodable {
    
    var airac: Int?
    var revision: Int?
    var url: String?
    
    var title: String {
        if let dec=airac, let part=revision {
            return "Airac \(dec) rev.\(part)"
        } else {
            return "*"
        }
    }
    
    var index: String {
        if let dec=airac, let part=revision {
            return String(dec*10+part)
        } else {
            return "*"
        }
    }
    var indexInt: Int {
        if let dec=airac, let part=revision {
            return dec*10+part
        } else {
            return 0
        }
    }
    
    
}
