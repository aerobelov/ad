//
//  databaseInfoRow.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 30.01.2022.
//

import Foundation

struct localDatabaseVersion: Decodable {
    var start_date: Int?
    var end_date: Int?
    var airac: Int?
    var revision: Int?
    
    var title: String {
        guard airac != nil, revision != nil else { return "" }
        return String("AIRAC \(airac!) rev.\(revision!)")
    }
    
    var indexInt: Int {
        if let dec=airac, let part=revision {
            return dec*10+part
        } else {
            return 0
        }
    }
    
    var Gregorian_start: String? {
        guard self.start_date != nil else { return nil }
        let d = NSDate(timeIntervalSince1970: TimeInterval(start_date!))
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-YY"
        return formatter.string(from: d as Date)
    }
    
    var Gregorian_end: String? {
        guard self.end_date != nil else { return nil }
        let d = NSDate(timeIntervalSince1970: TimeInterval(end_date!))
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-YY"
        return formatter.string(from: d as Date)
    }
}
