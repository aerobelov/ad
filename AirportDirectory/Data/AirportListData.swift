//
//  AirportListData.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import Foundation

class AirportListData: Decodable, ObservableObject {
    var apt_icao: String?
    var apt_name: String?
    var iata: String?
}


