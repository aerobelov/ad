//
//  AirportRow.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 07.01.2022.
//

import Foundation

struct AirportRow: Decodable {
    
    //Location Chapter
    var iata: String?
    var apt_icao: String?
    var apt_name: String?
    var loc_name: String?
    var count_name: String?
    var province: String?
    var iso_code: String?
    var ifr_vfr: String?
    var latitude: String?
    var longitude: String?
    var timezone: String?
    var timeadjust: Int?
   
    //General chapter
    var elevation: Int?
    var `var`: String?
    var surface: String?
    var rwy_cat: String?
    var long_rwy: Int?
    var fir: String?
    var apt_entry: String?
    var customs: String?
    var fire: String?
    var summertime: String?
   
}
