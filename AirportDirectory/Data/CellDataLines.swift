//
//  ListViewStructures.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 09.01.2022.
//

import Foundation

struct plainLine: CellDataProtocol {
    
    var title: String
    var initial: String
    
    init(title: String, initial: String) {
        self.title = title
        self.initial = initial
    }
    
    func prepare(set: SettingsSet) -> String {
        return self.initial
    }
    
    
}

struct distanceLine: CellDataProtocol {
    
    var title: String
    var initial: String
    
    init(title: String, initial: String) {
        self.title = title
        self.initial = initial
    }
    
    func prepare(set: SettingsSet) -> String {
        if let v = Double(self.initial) {
            let unit = set.unit.rawValue
            let result = v * unit
            return String("\(Int(result)) \(set.unit.id)")
        } else {
            return "ERR"
        }
    }
}

struct timezoneLine: CellDataProtocol {
    
    var title: String
    var initial: String
    
    init(title: String, initial: String) {
        self.title = title
        self.initial = initial
    }
    
    func prepare(set: SettingsSet) -> String {
        return "UTC=LOCAL\(self.initial)"
    }
}

struct runwayHeadingLine: CellDataProtocol {
    
    var title: String
    var initial: String
    
    init(title: String, initial: String) {
        self.title = title
        self.initial = initial
    }
    
    func prepare(set: SettingsSet) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.minimumIntegerDigits = 3
        if let value = Double(self.initial) {
            let result = NSNumber(value: value/10)
            return formatter.string(from: result) ?? ""
        } else {
            return self.initial
        }
        
    }
}

struct coordinateLine: CellDataProtocol {
    
    var title: String
    var initial: String
    
    init(title: String, initial: String) {
        self.title = title
        self.initial = initial
    }
    
    func prepare(set: SettingsSet) -> String {
        let lprefix = self.initial.prefix(1)
        let secdec = self.initial.suffix(2)
        let coordNoPrefix = self.initial.dropFirst()
        let coordNoDec = coordNoPrefix.dropLast(2)
        let sec = coordNoDec.suffix(2)
        let coordNoSec = coordNoDec.dropLast(2)
        let min = coordNoSec.suffix(2)
        let deg = coordNoSec.dropLast(2)
        switch set.coord {
        case .ddmm:
            var mindec = 0.0
            if let dm=Double(min), let ds=Double(sec), let dsd=Double(secdec) {
                let floatsec = ds + dsd/100
                let floatmin = Double(round((floatsec / 60)*100) / 100)
                mindec = dm + floatmin
            }
            return "\(lprefix)\(deg)\u{00B0}\(mindec)\u{2032}"
        case .ddmmss:
            return "\(lprefix)\(deg)\u{00B0}\(min)\u{2032}\(sec).\(secdec)\u{2033}"
        }
    }
}

