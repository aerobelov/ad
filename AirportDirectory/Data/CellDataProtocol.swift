//
//  CellDataProtocol.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 28.01.2022.
//

import Foundation

protocol CellDataProtocol {
    var title: String {get set}
    func prepare(set: SettingsSet) -> String
}
