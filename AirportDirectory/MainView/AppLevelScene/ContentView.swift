//
//  ContentView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.scenePhase) var scenePhase
    @ObservedObject var appModel: AppModel = AppModel()
    
    var body: some View {
        NavigationView {
            MasterView(selectedAirportIcaoCode: "UUWW")
            DetailView(icao: "UUWW")
        }
        .environmentObject(appModel)
        .navigationViewStyle(DoubleColumnNavigationViewStyle())
        .onChange(of: scenePhase, perform: { newphase in
            if newphase == .active {
                self.appModel.getSaoVersion()
            }
           
        })
    }
}
