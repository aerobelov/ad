//
//  MainModel.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 05.01.2022.
//

import Foundation
import SwiftUI
import Combine
import Reachability
import Network

class AppModel: ObservableObject,  Reloadable {
    
    @Published var updateAvailable: Bool = false
    @Published var serverVersion: ServerVersion?
    @Published var settingsArePresented: Bool = false
    @Published var localDatabaseVersion: localDatabaseVersion?
    @Published var sheetActive: Bool = false
    var subscriptions = Set<AnyCancellable>()
    var localDatabaseInfoReader = LocalDatabaseInfoReader()
    
    init() {
        getSaoVersion()
        getLocalVersion()
    }
    
    func getLocalVersion() {
        localDatabaseInfoReader.fetchInfoData { [weak self] info in
            DispatchQueue.main.async {
                self?.localDatabaseVersion = info
                self?.checkVersions()
            }
        }
    }
    
    func checkVersions() {
        if let server = self.serverVersion?.indexInt, let local = self.localDatabaseVersion?.indexInt {
            DispatchQueue.main.async {
                self.updateAvailable = server > local
            }
        }
    }
    
    func getSaoVersion() {
        let updater = UpdatesChecker()
        updater.updatesPublisher()
            .receive(on: RunLoop.main)
            .sink { [weak self] element in
                self?.serverVersion = element
                self?.checkVersions()
            }
            .store(in: &subscriptions)
    }
}
