//
//  SearchBar.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 02.01.2022.
//

import SwiftUI

struct SearchBar: View {
    
    @State private var isEditing = false
    @Binding var searchText: String
    
    var body: some View {
        HStack {
            TextField("Search ICAO or IATA code", text: $searchText)
                .autocapitalization(.allCharacters)
                .disableAutocorrection(true)
                .padding(7)
                .padding(.horizontal, 15)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .padding(.horizontal, 10)
                .onTapGesture {
                    isEditing = true
                }
            if isEditing {
                Button(action: {
                    isEditing = false
                    searchText = ""
                })
                {
                    Text("Cancel")
                }
                .padding(.trailing, 10)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
   @State static var mysearchText: String = ""
    static var previews: some View {
        SearchBar(searchText: $mysearchText)
    }
}
