//
//  MasterView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import SwiftUI

struct MasterView: View {
    
    @ObservedObject var masterModel = MasterViewModel()
    @State  var selectedAirportIcaoCode: String
    
    init(selectedAirportIcaoCode: String) {
        self.selectedAirportIcaoCode = selectedAirportIcaoCode
    }
    
    var body: some View {
        VStack {
            SearchBar(searchText: $masterModel.searchText)
            List(masterModel.airports, id: \.apt_icao) { item in
                NavigationLink(destination: DetailView(icao: item.apt_icao ?? "")) {
                    VStack(alignment: .leading) {
                        HStack {
                            Text(item.apt_name ?? "")
                                .font(.system(.subheadline))
                        }
                        HStack {
                            Text("\(item.apt_icao ?? "")")
                                .font(.system(.body))
                                .fontWeight(.bold)
                            Spacer()
                            Text("\(item.iata ?? "")")
                                .font(.system(.body))
                                .fontWeight(.bold)
                        }
                    }
                }
            }
            .navigationTitle("Airport selection")
            .navigationBarTitleDisplayMode(.large)
            .listStyle(.sidebar)
        }
        
    }
}
//
//struct MasterView_Previews: PreviewProvider {
//    static var previews: some View {
//        MasterView()
//    }
//}
