//
//  MasterViewModel.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import Foundation
import SwiftUI
import Combine

class MasterViewModel: ObservableObject {
    
    @Published var airports: [AirportListData] = []
    @Published var searchText: String = ""
    var searchSubscription: AnyCancellable?
    
    var manager = AirportListDatabaseManager()
    
    init() {
        listen()
    }
    
    func listen() {
        searchSubscription =
        self.$searchText
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .receive(on: RunLoop.main)
            .sink {
                self.airports = []
                self.manager.searchArportsList(text: $0) { [weak self] arp in
                    self?.airports.append(contentsOf: arp)
                }
            }
    }
    
}
