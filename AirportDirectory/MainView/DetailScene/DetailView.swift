//
//  DetailView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 01.01.2022.
//

import SwiftUI

struct DetailView: View {
    
    @ObservedObject var detailModel: DetailViewModel
    @EnvironmentObject var appModel: AppModel
    
    init(icao: String) {
        detailModel = DetailViewModel(icao: icao)
    }
    
    var body: some View {
        
        List {
            Section(header: Text("Status")) {
                StatusView()
            }
            Section(header: Text("Location info")) {
                DetailChapterView(row: detailModel.locationData!)
            }
            Section(header: Text("General info")){
                DetailChapterView(row: detailModel.generalData!)
            }
            Section(header: Text("Runway information")) {
                RunwayListView(runways: detailModel.runways ?? [])
            }
        }
        .environmentObject(detailModel)
        .navigationBarItems(trailing: DetailToolBarItemsView().environmentObject(detailModel))
        .navigationTitle(String("Detailed info"))
        .navigationBarTitleDisplayMode(.large)
        .sheet(isPresented: $detailModel.updateSheetIsPresented, onDismiss: {
        }, content: {
            UpdateSheetView()
                .environmentObject(detailModel)
        })
    }
}

//struct DetailView_Previews: PreviewProvider {
//    @State var icao: String = "UUWW"
//
//    static var previews: some View {
//        DetailView(icao: icao)
//    }
//}
