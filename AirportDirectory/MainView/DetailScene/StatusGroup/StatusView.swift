//
//  StatusView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 31.01.2022.
//

import SwiftUI

struct StatusView: View {
    
    @EnvironmentObject var appModel: AppModel
    @EnvironmentObject var detailModel: DetailViewModel
    
    var body: some View {
        HStack{
            Text("\(appModel.localDatabaseVersion?.title ?? "")")
            Text(String("\(appModel.localDatabaseVersion?.Gregorian_start ?? "")"))
                .fontWeight(.semibold)
            Text("-")
            Text(String("\(appModel.localDatabaseVersion?.Gregorian_end ?? "")"))
                .fontWeight(.semibold)
            Spacer()
            ServerStatusView()
                .opacity(appModel.updateAvailable ? 1 : 0)
        }
    }
}

