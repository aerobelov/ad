//
//  ServerStatusView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 28.03.2022.
//

import SwiftUI

struct ServerStatusView: View {
    @EnvironmentObject var detailModel: DetailViewModel
    @EnvironmentObject var appModel: AppModel
    
    var body: some View {
        
        Text(String("On server:  \(appModel.serverVersion?.title ?? "")"))
          
        
        Button(action: {
            detailModel.updateSheetIsPresented.toggle()
        } , label: {
            Text("Update")
                .padding(5)
                .background(Color.red)
                .foregroundColor(Color.white)
                .cornerRadius(10)
            
        })
            .buttonStyle(BorderlessButtonStyle())
           
            
    }
}

struct ServerStatusView_Previews: PreviewProvider {
    static var previews: some View {
        ServerStatusView()
    }
}
