//
//  SettingsView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 19.01.2022.
//

import SwiftUI

struct SettingsView: View {
    
    @EnvironmentObject var appModel: AppModel
    @EnvironmentObject var detailModel: DetailViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Picker("Units", selection: $detailModel.set.unit) {
                ForEach(DistanceUnits.allCases) { element in
                    Text("\(element.id)").tag(element)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            Spacer()
            Picker("Units", selection: $detailModel.set.coord) {
                ForEach(CoordinateUnits.allCases) { element in
                    Text("\(element.id)").tag(element)
                }
            }
            .pickerStyle(SegmentedPickerStyle())
        }
        .padding()
    }
}

//struct SettingsView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingsView()
//    }
//}
