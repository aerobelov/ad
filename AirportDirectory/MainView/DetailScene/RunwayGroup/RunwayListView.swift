//
//  RunwayListView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 10.01.2022.
//

import SwiftUI

struct RunwayListView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    var runways: [RunwayRow] = [RunwayRow(icao: "XXXX")]
    
    var body: some View {
        ForEach (runways, id:\.rw) { runway in
            NavigationLink(destination: RunwayDetailView(runway: runway).environmentObject(detailModel) ) {
                HStack {
                    Text(runway.rw ?? "")
                        .fontWeight(.semibold)
                }
            }
        }
    }
}

//struct RunwayListView_Previews: PreviewProvider {
//    static var previews: some View {
//        RunwayListView()
//    }
//}
