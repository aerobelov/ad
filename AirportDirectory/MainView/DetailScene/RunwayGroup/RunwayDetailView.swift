//
//  RunwayDetailView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 10.01.2022.
//

import SwiftUI

struct RunwayDetailView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    
    var runway: RunwayRow = RunwayRow(icao: "UUWW")
    var presenter = RunwayPresenter()
    
    var body: some View {
        List{
            ForEach(presenter.getRunwayData(runway: runway), id:\.title ) { line in
                HStack {
                    Text(line.title)
                        .fontWeight(.semibold)
                    Spacer()
                    Text(line.prepare(set: detailModel.set))
                }
            }
        }
        .navigationTitle("Runway details")
        .navigationBarTitleDisplayMode(.large)
    }
}
//
//struct RunwayDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        RunwayDetailView()
//    }
//}
