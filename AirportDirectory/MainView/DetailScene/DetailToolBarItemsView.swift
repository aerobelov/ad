//
//  DetailToolBarItemsView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 30.01.2022.
//

import SwiftUI



struct DetailToolBarItemsView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    
    var body: some View {
        Button(action: {
            detailModel.settingsArePresented = true
        }, label: {
            Image(systemName: "gearshape")
        })
        .popover(isPresented: $detailModel.settingsArePresented) {
            SettingsView()
        }
    }
}

//struct DetailToolBarItemsView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailToolBarItemsView()
//    }
//}
