//
//  DetailViewModel.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 06.01.2022.
//

import Foundation
import SwiftUI
import Zip
import SQLite
import SQLite3


class DetailViewModel: NSObject, ObservableObject, URLSessionDelegate, URLSessionDownloadDelegate {
    
    var icao: String?
    //Controls and interface
    @Published var settingsArePresented: Bool = false
    @Published var updateSheetIsPresented: Bool = false
    @Published var errorInDownloading: Bool = false
    @Published var progress: Double = 0.0
    
    //Database information
    @Published var locationData: [CellDataProtocol]?
    @Published var generalData: [CellDataProtocol]?
    @Published var runways: [RunwayRow]?
    @Published var presenter: Presenter?
    
    //Unit Settings for detailView
    @Published var set: SettingsSet = SettingsSet(unit: .meter, coord: .ddmmss)
    
    //Update vars
    @Published var downloadTask: URLSessionDownloadTask?
    var appModelAgent: Reloadable?
    

    init(icao: String) {
        super.init()
        self.icao = icao
        self.presenter = Presenter()
        self.fetchData()
    }
    
    func fetchData() {
        let manager = DirectoryManager()
        let runwayManager = RunwayManager()
        manager.fetchAirportData(icaoCode: self.icao!) { [weak self] row in
            self?.locationData = self?.presenter?.getLocationData(source: row)
            self?.generalData = self?.presenter?.getGeneralData(source: row)
        }
        runwayManager.fetchRunwayData(icaoCode: self.icao!, completion: { [weak self] runways in
            self?.runways = runways
        })
    }
    
    func update(urlString: String, agent: Reloadable) {
        guard let validURL = URL(string: urlString) else {
            return
        }
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        downloadTask = session.downloadTask(with: validURL)
        self.appModelAgent = agent
        downloadTask!.resume()
    }
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Double(totalBytesWritten)/Double(totalBytesExpectedToWrite)
        DispatchQueue.main.sync {
            self.progress = progress
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let fileReplacer = DatabaseReplacer()
            fileReplacer.replace(from: location, completion: { result in
                DispatchQueue.main.async {
                    switch result {
                        case .success(_):
                            self.updateSheetIsPresented = false
                            self.errorInDownloading = false
                            self.appModelAgent?.getLocalVersion()
                        case .failure(_):
                            self.errorInDownloading = true
                    }
                }
            })
    }
    
    
}

protocol Reloadable {
    func getLocalVersion()
}
