//
//  ProgressView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 27.03.2022.
//

import SwiftUI

struct ProgressView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    let style = StrokeStyle(lineWidth: 20, lineCap: .round)
    let color = Color.blue
    let colorbg = Color.blue.opacity(0.3)
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(
                    AngularGradient(gradient: .init(colors: [colorbg]), center: .center),
                style: style)
            Circle()
                .trim(from: 0.0, to: CGFloat(detailModel.progress))
                .stroke(
                    AngularGradient(gradient: .init(colors: [color]), center: .center),
                style: style)
                .rotationEffect(Angle(degrees: 270))
                .animation(.default)
            Text(String("\(Int(detailModel.progress*100)) %"))
                .fontWeight(.bold)
        }
        .frame(width: 100, height: 100, alignment: .center)
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView()
    }
}
