//
//  UpdateSheetView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 03.02.2022.
//

import SwiftUI

struct UpdateSheetView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    @EnvironmentObject var appModel: AppModel
    
    var body: some View {
        VStack{
            Text("Update available \(self.appModel.serverVersion?.title ?? "")")
            Spacer()
            ProgressView()
            Spacer()
            Button(action: {
                detailModel.update(urlString: appModel.serverVersion?.url ?? "", agent: appModel)
            }, label: {
                Text("Start update")
                    .padding(12)
                    .background(Color.blue)
                    .foregroundColor(Color.white)
                    .cornerRadius(10)
            })
            Spacer()
            if detailModel.errorInDownloading == true {
                Text ("Network Error")
            }
        }
        .frame(width: 400, height: 400, alignment: .center)
    }
}

//struct UpdateSheetView_Previews: PreviewProvider {
//    static var previews: some View {
//        UpdateSheetView()
//    }
//}
