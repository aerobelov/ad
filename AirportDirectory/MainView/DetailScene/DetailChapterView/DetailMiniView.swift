//
//  DetailMiniView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 23.01.2022.
//

import SwiftUI

struct DetailMiniView: View {
    
    @EnvironmentObject var detailModel: DetailViewModel
    
    var line: CellDataProtocol
    
    var body: some View {
        HStack {
            Text(line.title)
                .fontWeight(.semibold)
            Spacer()
            Text(line.prepare(set: detailModel.set))
        }
    }
}

//struct DetailMiniView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailMiniView()
//    }
//}
