import Foundation

var coord = "N54485600"
let lprefix = coord.prefix(1)
let secdec = coord.suffix(2)
let coordNoPrefix = coord.dropFirst()
let coordNoDec = coordNoPrefix.dropLast(2)
let sec = coordNoDec.suffix(2)
let coordNoSec = coordNoDec.dropLast(2)
let min = coordNoSec.suffix(2)
let deg = coordNoSec.dropLast(2)


print("\(lprefix)\(deg)\u{00B0}\(min)\u{2032}\(sec).\(secdec)\u{2033}")
var mindec = 0.0
if let dm=Double(min), let ds=Double(sec), let dsd=Double(secdec) {
    let floatsec = ds + dsd/100
    let floatmin = round((floatsec / 60)*100) / 100
    mindec = dm + floatmin
}

print("\(lprefix)\(deg)\u{00B0}\(mindec)\u{2032}")


