//
//  LocationView.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 05.01.2022.
//

import SwiftUI

struct DetailChapterView: View {
    
    var row: [CellDataProtocol] = []
    
    var body: some View {
        ForEach(row, id:\.title ) { line in
            DetailMiniView(line: line)
        }
    }
        
}

struct LocationView_Previews: PreviewProvider {
  
    static var previews: some View {
        DetailChapterView()
           
    }
}
