//
//  UpdatesChecker.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 28.01.2022.
//

import Foundation
import Combine

enum NetworkError: Error {
    case VersionsError
}

class UpdatesChecker {
    
    var session: URLSession
    let url = URL(string: "https://cao.vda.ru/navdb/iead2/versions.json")
    
    init() {
        let config = URLSessionConfiguration.default
        let userPassData = "vdaefb:Li45udgjYF6"
        let pswData = userPassData.data(using: .utf8)!
        let base64encodedData = pswData.base64EncodedString()
        let authString = "Basic \(base64encodedData)"
        config.httpAdditionalHeaders = ["Authorization": authString]
        config.timeoutIntervalForResource = 60
        config.urlCache = nil
        self.session = URLSession(configuration: config)
    }
    
    
    
    func updatesPublisher() -> AnyPublisher<ServerVersion, Never> {
       
        return self.session.dataTaskPublisher(for: self.url!)
            .tryMap { element -> Data in
                guard let response = element.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw NetworkError.VersionsError
                }
                return element.data
            }
            .decode(type: ServerVersion.self, decoder: JSONDecoder())
            .replaceError(with: ServerVersion())
            .eraseToAnyPublisher()
    }
    
}
