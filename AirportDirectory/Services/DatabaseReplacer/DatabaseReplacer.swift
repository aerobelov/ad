//
//  DatabaseReplacer.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 30.03.2022.
//

import Foundation
import Zip

struct DatabaseReplacer {
    let localReader: LocalDataBaseReader
    
    init() {
        localReader = LocalDataBaseReader()
    }
    
    func replace(from location: URL, completion: (Result<String, Error>)->Void) {
        
        let documents = self.localReader.documentsDirectory
        let tempZipUrl = documents.appendingPathComponent("tempBase.zip")
        
        do {
            //IF PREVIOUSLY DOWNLOADED ARCHIVE EXISTS - REMOVE IT
            if FileManager.default.fileExists(atPath: tempZipUrl.path) {
                try FileManager.default.removeItem(at: tempZipUrl)
            }
            try FileManager.default.moveItem(at: location, to: tempZipUrl)
            do {
                try  Zip.unzipFile(tempZipUrl, destination: documents, overwrite: true, password: nil)
                completion(.success("Success"))
            } catch {
                completion(.failure(error))
            }
        } catch {
            print (error.localizedDescription)
        }
    }
}
