//
//  DirectoryManager.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 06.01.2022.
//

import Foundation
import SQLite

class DirectoryManager {
    
    let reader: LocalDataBaseReader = LocalDataBaseReader()
    var row: AirportRow?
    
    func fetchAirportData(icaoCode: String, completion: (_ row: AirportRow) -> Void ) {
        if let path = reader.databaseLocationPath() {
            
            let apt_dir = Table("apt_dir")
            
            let icao = Expression<String>("apt_icao")
            
            do {
                let db = try Connection(path, readonly: true)
                let data: [AirportRow] = try db.prepare(apt_dir.filter( icao == icaoCode )).map { row in
                    return try row.decode()
                }
                
                if data.count > 0 {
                    completion(data.first!)
                }
            } catch {
                print("NOTFOUND")
            }
        }
    }
    
    
    
}
