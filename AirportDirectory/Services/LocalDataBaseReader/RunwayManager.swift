//
//  RunwayManager.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 10.01.2022.
//

import Foundation
import SQLite

class RunwayManager {
    
    let reader: LocalDataBaseReader = LocalDataBaseReader()
    var row: RunwayRow?
    
    func fetchRunwayData(icaoCode: String, completion: (_ rows: [RunwayRow]) -> Void ) {
        if let path = reader.databaseLocationPath() {
            
            let runways = Table("rw_thres")
            let icao = Expression<String>("icao")
            
            do {
                let db = try Connection(path, readonly: true)
                let data: [RunwayRow] = try db.prepare(runways.filter( icao == icaoCode )).map { row in
                    return try row.decode()
                }
                
                if data.count > 0 {
                    completion(data)
                }
            } catch {
                print("NOTFOUND")
            }
        }
    }
    
}
