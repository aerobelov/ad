//
//  DataBaseManager.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 02.01.2022.
//

import Foundation
import SQLite
import Combine

class AirportListDatabaseManager {
    
    let reader = LocalDataBaseReader()
    
    
    init() {
        searchArportsList(text: "", completion: {airports in })
    }
    
    func searchArportsList(text: String, completion: (_ airports: [AirportListData]) -> Void)  {
        if let path = reader.databaseLocationPath() {
            var resultArray = [AirportListData]()
            let apt_dir = Table("apt_dir")
            let icao = Expression<String>("apt_icao")
            let name = Expression<String>("apt_name")
            let iata = Expression<String>("iata")
            let rwy = Expression<Int64>("long_rwy")
            
            do {
                let db = try Connection(path, readonly: true)
                
                if text != "" {
                    resultArray = try db.prepare(apt_dir.filter(( icao.like("%\(text)%") ||
                                                                  iata.like("%\(text)%") ||
                                                                  name.like("%\(text)%")
                                                                ) && (icao.length == 4 && rwy > 5000))).map { row in
                        return try row.decode()
                    }
                } else {
                    resultArray = try db.prepare(apt_dir.filter( icao.length == 4 && rwy > 10000 )).map { row in
                        return try row.decode()
                    }
                }
                if resultArray.count > 0 {
                    completion(resultArray)
                }
            } catch {
                print("NOTFOUND")
            }
            
        } else {
            completion([])
        }
       
    }
    
    
}
