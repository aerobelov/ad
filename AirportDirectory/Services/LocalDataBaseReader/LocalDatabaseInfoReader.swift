//
//  InfoReader.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 30.01.2022.
//

import Foundation
import SQLite

class LocalDatabaseInfoReader {
    
    let reader: LocalDataBaseReader = LocalDataBaseReader()
    
    let airac = Expression<Int>("airac")
    let revision = Expression<Int>("revision")
    let start_date = Expression<Int>("start_date")
    let end_date = Expression<Int>("end_date")
    
    func fetchInfoData(completion: (_ info: localDatabaseVersion) -> Void ) {
        if let path = reader.databaseLocationPath() {
            
            let infoTable = Table("info")
           
            do {
                let db = try Connection(path, readonly: true)
                let infoRow: [localDatabaseVersion] = try db.prepare(infoTable).map { row in
                    return try row.decode()
                }
                
                if infoRow.count > 0 {
                    completion(infoRow.first!)
                }
            } catch {
                print("NOTFOUND")
            }
        }
    }
    
}
