//
//  DBReadError.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 02.01.2022.
//

import Foundation

enum DBReadError: Error {
    case notExists
}

enum DownloadError: Error {
    case downloadError
}
