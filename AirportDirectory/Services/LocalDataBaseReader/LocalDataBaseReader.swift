//
//  LocalDataBaseReader.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 02.01.2022.
//

import Foundation
 
class LocalDataBaseReader {
    
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    func databaseLocationPath() -> String? {
       return copyFromBundleIfNotExist()
    }
    
    //GET DOCUMENTS PATH
    private func getDataBaseAtDocumentsDirectoryPath() -> String? {
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let destinationPath = documents + "/database.sqlite"
        return destinationPath
    }
    
    //CHECK IF DATABASE EXISTS AT DOCUMENTS FOLDER 
    private func dataBaseExistInDocuments() -> Bool {
        let destinationPath = getDataBaseAtDocumentsDirectoryPath()
        guard destinationPath != nil else { return false }
        return FileManager.default.fileExists(atPath: destinationPath!)
    }
    
    //IF DATABASE IS MISSING AT DOCUMENTS FOLDER - COPY TO DOCUMENTS FOLDER FROM GIVEN LOCATION
    private func copyDatabaseIfNeeded(sourcePath: String,  completion: @escaping ( Result<String, DBReadError>) -> Void ) {
        
        //NAME DOCUMENTSDIRECTORY PATH FOR DATABASE
        if let destinationPath = getDataBaseAtDocumentsDirectoryPath() {
            
            //ENSURE DB NOT EXIST ELSE RETURN DOC DIRECTORY PATH
            guard !dataBaseExistInDocuments() else { return completion(.success(destinationPath)) }
            do {
                try FileManager.default.copyItem(atPath: sourcePath, toPath: destinationPath)
                completion(.success(destinationPath))
            } catch {
                completion(.failure(.notExists))
            }
        }
        
    }
    
    //GET DATABASE FROM BUNDLE LOCATION AND COPY TO DOCUMENTS FOLDER IF NEED SO
    private func copyFromBundleIfNotExist() -> String? {
        let sourcePath = Bundle.main.path(forResource: "database", ofType: "sqlite")
        guard sourcePath != nil else { return nil }
        var dbpath:String?
        
        copyDatabaseIfNeeded(sourcePath: sourcePath!) { result in
            switch result {
            case .success(let path):
                dbpath = path
            case .failure(_):
                dbpath = nil
            }
        }
        return dbpath
    }
    
}
