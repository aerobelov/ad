//
//  DatabaseDownloader.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 03.02.2022.
//

import Foundation

class DatabaseDownloader: ObservableObject {
    
    var session: URLSession
    
    init() {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForResource = 60
        config.urlCache = nil
        self.session = URLSession(configuration: config)
    }
    
    func download(from url: URL, completion: @escaping (Result<String, DownloadError>) -> Void ) {
        
    }
    
}
