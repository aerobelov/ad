//
//  RunwayDataPresenter.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 11.01.2022.
//

import Foundation

struct RunwayPresenter {
    
    func getRunwayData(runway: RunwayRow) -> [CellDataProtocol] {
        var result = [CellDataProtocol]()
        if let icao = runway.icao, icao != "" {
            result.append(plainLine(title: "ICAO", initial: icao))
        }
        if let rw = runway.rw, rw != "" {
            result.append(plainLine(title: "Runway", initial: rw))
        }
        if let len = runway.len {
            result.append(distanceLine(title: "Length", initial: String(len)))
        }
        if let width = runway.width {
            result.append(plainLine(title: "Width", initial: String(width)))
        }
        if let mc = runway.mc, mc != "" {
            result.append(runwayHeadingLine(title: "Magnetic Heading", initial:  mc))
        }
        if let lat = runway.lat, lat != "" {
            result.append(coordinateLine(title: "Latitude", initial: lat))
        }
        if let lon = runway.lon, lon != "" {
            result.append(coordinateLine(title: "Longitude", initial: lon))
        }
        if let elev = runway.elev {
            result.append(distanceLine(title: "Elevation", initial: String(elev)))
        }
        if let displaced = runway.dsplcd_thr {
            result.append(distanceLine(title: "Displaced threshould", initial: String(displaced)))
        }
        if let stop = runway.stopway {
            result.append(distanceLine(title: "Stopway", initial: String(stop)))
        }
        if let slope = runway.slope, slope != "" {
            result.append(plainLine(title: "Runway slope", initial: slope))
        }
       return result
    }
}
