//
//  LocationPresenter.swift
//  AirportDirectory
//
//  Created by Pavel Belov on 07.01.2022.
//

import Foundation
import SwiftUI

struct Presenter {
    
    func getLocationData(source: AirportRow) -> [CellDataProtocol] {
        
        var result = [CellDataProtocol]()
        if let icao = source.apt_icao {
            result.append(plainLine(title: "ICAO", initial: icao))
        }
        if let iata = source.iata, iata != "" {
            result.append(plainLine(title: "IATA", initial: iata))
        }
        if let loc_name = source.loc_name, loc_name != "" {
            result.append(plainLine(title: "Location name", initial: loc_name))
        }
        if let apt_name = source.apt_name, apt_name != "" {
            result.append(plainLine(title: "Airport name", initial: apt_name))
        }
        if let country = source.count_name, country != "" {
            result.append(plainLine(title: "Country", initial: country))
        }
        if let state = source.province, state != "" {
            result.append(plainLine(title: "State", initial: state))
        }
        if let iso = source.iso_code, iso != "" {
            result.append(plainLine(title: "ISO", initial: iso))
        }
        if let lat = source.latitude, lat != "" {
            result.append(coordinateLine(title: "Latitude", initial: lat))
        }
        if let lon = source.longitude, lon != "" {
            result.append(coordinateLine(title: "Longitude", initial: lon))
        }
        if let tz = source.timezone, tz != "" {
            result.append(timezoneLine(title: "Timezone", initial: tz))
        }
        if let st = source.summertime, st != "" {
            result.append(plainLine(title: "Summertime", initial: st))
        }
        
        return result
    }
    
    func getGeneralData(source: AirportRow) -> [CellDataProtocol] {
        var result = [CellDataProtocol]()
        if let elev = source.elevation {
            result.append(distanceLine(title: "Elevation", initial: String(elev)))
        }
        if let variation = source.var, variation != "" {
            result.append(plainLine(title: "Magnetic variation", initial: variation))
        }
        if let surf = source.surface, surf != "" {
            result.append(plainLine(title: "Surface", initial: surf))
        }
        if let ils = source.rwy_cat, ils != "" {
            result.append(plainLine(title: "Runway category", initial: ils))
        }
        if let longest = source.long_rwy {
            result.append(distanceLine(title: "Longest runway", initial: String(longest)))
        }
        if let fir = source.fir, fir != "" {
            result.append(plainLine(title: "FIR", initial: fir))
        }
        if let entry = source.apt_entry, entry != "" {
            result.append(plainLine(title: "Airport of entry", initial: entry))
        }
        if let customs = source.customs, customs != "" {
            result.append(plainLine(title: "Customs", initial: customs))
        }
        if let fire = source.fire, fire != "" {
            result.append(plainLine(title: "Fire category", initial: fire))
        }
        return result
    }
}
